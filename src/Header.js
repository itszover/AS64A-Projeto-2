export default function Header() {
    return (
        <header>
            <nav className="navbar">
                <h1 className="title">Yu-Gi-Oh! API</h1>
                <span className="nav-span">
                    <a href="https://ygoprodeck.com/api-guide/" target="_blank"> API utilizada no projeto</a>
                </span>
            </nav>
        </header>
    )
}