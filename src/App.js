import './App.css';
import Header from './Header.js';
import InfoSection from './InfoSection.js';

function App() {
  return (
    <>
    <Header />
    <main>
      <InfoSection />
    </main>
    </>
  );
}

export default App;
